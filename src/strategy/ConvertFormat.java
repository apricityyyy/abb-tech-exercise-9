package strategy;

// common interface for file format to be converted
public interface ConvertFormat {
    // the different classes, methods, etc. used for conversions
    // will make the strategies different; however, I have not
    // actually converted files, I just printed out messages
    boolean convertFile();
    void getConvertFormat();
}