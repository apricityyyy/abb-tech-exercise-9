package strategy;

import java.util.Scanner;

public class StrategyDemo {
    private static final Scanner reader = new Scanner(System.in);
    private static final RequestConversion request = new RequestConversion();
    private static ConvertFormat strategy;

    public static void main(String[] args) {
        while(request.isContinues()) {
            if (strategy == null) {
                String textBlock = """
                        Please, select a desired conversion format:
                        1 - PDF
                        2 - DOCX""";
                System.out.println(textBlock);
                String choice = reader.nextLine();

                if (choice.equals("1")) {
                    strategy = new ConvertToPdf();
                } else {
                    strategy = new ConvertToDocx();
                }
            }

            request.processRequest(strategy);

            System.out.println("Convert current file or proceed further? C/P: ");
            String proceed = reader.nextLine();
            if (proceed.equalsIgnoreCase("C")) {
                if (strategy.convertFile()) {
                    System.out.println("Conversion has been successful.");
                } else {
                    System.out.println("FAIL! Please check your request.");
                }

                request.setContinues(false);
            }
        }
    }
}
