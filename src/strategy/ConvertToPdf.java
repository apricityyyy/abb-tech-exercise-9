package strategy;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

// Concrete strategy
public class ConvertToPdf implements ConvertFormat {
    private static final Map<String, String> possibleConversions = new HashMap<>();
    private final Scanner READER = new Scanner(System.in);
    private String fileName;

    // for most files, it is possible to convert to pdf, but these are made for testing purposes
    static {
        possibleConversions.put("docx", "pdf");
        possibleConversions.put("txt", "pdf");
        possibleConversions.put("pptx", "pdf");
    }

    @Override
    public void getConvertFormat() {
        System.out.println("Enter the full name of the file you want to convert to pdf: ");
        fileName = READER.nextLine();

        if (verify()) {
            System.out.println("Your file can be converted.");
        } else {
            System.out.println("The conversion is not possible.");
        }
    }

    private boolean verify() {
        String fileExtension;

        if (fileName == null || !fileName.contains(".")) {
            fileExtension = "";
        } else {
            fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
        }

        return "pdf".equalsIgnoreCase(possibleConversions.get(fileExtension));
    }

    @Override
    public boolean convertFile() {
        if (verify()) {
            System.out.println("Converting " + fileName + " to .pdf file.");
            // actual conversion can happen here
            return true;
        }

        return false;
    }
}