package strategy;

// doesn't know the exact strategy that has been picked
public class RequestConversion {
    private boolean continues = true;

    public void processRequest(ConvertFormat strategy) {
        strategy.getConvertFormat();
    }

    public boolean isContinues() {
        return continues;
    }

    public void setContinues(boolean continues) {
        this.continues = continues;
    }
}