package abstract_factory;

// "coffee" product variant 1
public class Espresso implements Coffee {
    @Override
    public void buy() {
        System.out.println("You bought a medium cup of espresso for 3.60 manats.");
    }

    @Override
    public void drink() {
        System.out.println("You can drink maximum 6 shots of espresso per day.");
    }
}
