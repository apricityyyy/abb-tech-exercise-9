package abstract_factory;

// abstract factory
public interface MarketFactory {
    Tea createTea();
    Coffee createCoffee();
}