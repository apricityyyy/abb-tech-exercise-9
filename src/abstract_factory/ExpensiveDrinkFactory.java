package abstract_factory;

// concrete factory #1
public class ExpensiveDrinkFactory implements MarketFactory {
    @Override
    public Tea createTea() {
        return new BlackTea();
    }

    @Override
    public Coffee createCoffee() {
        return new Americano();
    }
}
