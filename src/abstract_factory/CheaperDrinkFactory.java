package abstract_factory;

// concrete factory #2
public class CheaperDrinkFactory implements MarketFactory {
    @Override
    public Tea createTea() {
        return new GreenTea();
    }

    @Override
    public Coffee createCoffee() {
        return new Espresso();
    }
}
