package abstract_factory;

// "tea" product variant #1
public class BlackTea implements Tea {
    @Override
    public void buy() {
        System.out.println("You bought a black tea for 6 manats.");
    }

    @Override
    public void prepare() {
        System.out.println("You prepared a black tea for 3-5 minutes.");
    }
}
