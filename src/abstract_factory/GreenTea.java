package abstract_factory;

// "tea" product variant #2
public class GreenTea implements Tea {
    @Override
    public void buy() {
        System.out.println("You bought a green tea for 5 manats.");
    }

    @Override
    public void prepare() {
        System.out.println("You prepared a green tea for 1-2 minutes.");
    }
}
