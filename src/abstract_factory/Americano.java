package abstract_factory;

// "coffee" product variant 2
public class Americano implements Coffee {
    @Override
    public void buy() {
        System.out.println("You bought a medium cup of Americano for 4.60 manats.");
    }

    @Override
    public void drink() {
        System.out.println("You can drink maximum 3 cups of Americano per day.");
    }
}
