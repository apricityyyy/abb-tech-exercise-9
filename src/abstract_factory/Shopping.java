package abstract_factory;

// Client code
public class Shopping {
    private Tea tea;
    private Coffee coffee;

    // we do not care about the type of product, because we use the interface
    public Shopping(MarketFactory factory) {
        tea = factory.createTea();
        coffee = factory.createCoffee();
    }

    public void doShopping() {
        tea.buy();
        tea.prepare();
        coffee.buy();
        coffee.drink();
    }
}
