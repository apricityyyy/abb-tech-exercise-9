package abstract_factory;

public class AbstractFactoryDemo {
    // factory type is picked during runtime
    private static Shopping configure(String choice) {
        Shopping shop;
        MarketFactory factory;

        if (choice.equalsIgnoreCase("expensive")) {
            System.out.println("\tEXPENSIVE DRINK FACTORY:");
            factory = new ExpensiveDrinkFactory();
        } else {
            System.out.println("\tCHEAP DRINK FACTORY:");
            factory = new CheaperDrinkFactory();
        }

        shop = new Shopping(factory);
        return shop;
    }

    public static void main(String[] args) {
        Shopping shop = configure("expensive");
        shop.doShopping();
        System.out.println("============================================");

        System.out.println();

        Shopping anotherShop = configure("cheap");
        anotherShop.doShopping();
        System.out.println("============================================");
    }
}