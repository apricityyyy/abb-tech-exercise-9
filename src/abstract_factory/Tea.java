package abstract_factory;

// a family of "tea" products
public interface Tea {
    void buy();
    void prepare();
}
