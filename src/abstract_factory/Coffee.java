package abstract_factory;

// a family of "coffee" products
public interface Coffee {
    void buy();
    void drink();
}
